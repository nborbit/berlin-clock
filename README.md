Berlin Clock completed test assignment

Maven needs to be installed to compile the application. 
Use the following command 'mvn clean install'

All logic located in class com.inkglobal.test.BerlinClock
The application supplied with unit tests. Please use 'mvn clean test' command to run them.
