package com.inkglobal.test;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 */
public class BerlinClockTest {

    private BerlinClock berlinClock = new BerlinClock();

    @Test
    public void testConvert1() throws Exception {
        Assert.assertEquals("Y OOOO OOOO OOOOOOOOOOO OOOO", berlinClock.convert("00:00:00"));
    }

    @Test
    public void testConvert2() throws Exception {
        Assert.assertEquals("O RROO RRRO YYROOOOOOOO YYOO", berlinClock.convert("13:17:01"));
    }

    @Test
    public void testConvert3() throws Exception {
        Assert.assertEquals("O RRRR RRRO YYRYYRYYRYY YYYY", berlinClock.convert("23:59:59"));
    }

    @Test
    public void testConvert4() throws Exception {
        Assert.assertEquals("Y RRRR RRRR OOOOOOOOOOO OOOO", berlinClock.convert("24:00:00"));
    }

    @Test
    public void testConvert5() throws Exception {
        Assert.assertEquals("Y RRRR OOOO YOOOOOOOOOO YOOO", berlinClock.convert("20:06:00"));
    }

    @Test
    public void testConvert6() throws Exception {
        Assert.assertEquals("Y OOOO OOOO YYOOOOOOOOO YYYY", berlinClock.convert("00:14:00"));
    }

    @Test(expected = RuntimeException.class)
    public void testConvertFail1() throws Exception {
        berlinClock.convert(null);
    }

    @Test(expected = RuntimeException.class)
    public void testConvertFail2() throws Exception {
        berlinClock.convert("no:no");
    }

    @Test(expected = RuntimeException.class)
    public void testConvertFail3() throws Exception {
        berlinClock.convert("no:no:no");
    }

    @Test(expected = RuntimeException.class)
    public void testConvertFail4() throws Exception {
        berlinClock.convert("123:444:333");
    }


}
