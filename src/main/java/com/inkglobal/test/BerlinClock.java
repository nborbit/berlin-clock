package com.inkglobal.test;

/**
 *
 */
public class BerlinClock {

    private String formatErrorMessage = "Wrong data format, please enter date in format 'hh:mm:ss'";

    private static final String YELLOW = "Y";
    private static final String RED = "R";
    private static final String OFF = "O";

    public String convert(String time) {

        if(time == null || !time.contains(":")) {
            throw new RuntimeException(formatErrorMessage);
        }
        String[] split = time.split(":");
        if(split.length != 3) {
            throw new RuntimeException(formatErrorMessage);
        }

        try {
            int seconds = Integer.parseInt(split[2]);
            int minutes = Integer.parseInt(split[1]);
            int hours = Integer.parseInt(split[0]);

            return seconds(seconds) + " " + hours(hours) + " " + minutes(minutes);

        } catch (NumberFormatException e) {
            throw new RuntimeException(formatErrorMessage, e);
        }


    }

    private String seconds(int seconds) {
        if(seconds < 0 || seconds > 59) {
            throw new RuntimeException("Wrong seconds");
        }
        return (seconds % 2 == 0) ? YELLOW : OFF;
    }

    private String hours(int hours) {
        if(hours < 0 || hours > 24) {
            throw new RuntimeException("Wrong hours");
        }

        StringBuilder result = new StringBuilder();

        int row1 = hours / 5;
        int row2 = hours - row1 * 5;

        for(int i = 0; i < 4; i++) {
            result.append(i < row1 ? RED : OFF);
        }

        result.append(" ");

        for(int i = 0; i < 4; i++) {
            result.append(i < row2 ? RED : OFF);
        }
        return result.toString();
    }

    private String minutes(int minutes) {
        if(minutes < 0 || minutes > 59) {
            throw new RuntimeException("Wrong minutes");
        }

        StringBuilder result = new StringBuilder();

        int row1 = minutes / 5;
        int row2 = minutes - row1 * 5;

        for(int i = 1; i <= 11; i++) {
            if(i % 3 == 0) {
                result.append(i <= row1 ? RED : OFF);
            } else {
                result.append(i <= row1 ? YELLOW : OFF);
            }
        }

        result.append(" ");

        for(int i = 0; i < 4; i++) {
            result.append(i < row2 ? YELLOW : OFF);
        }

        return result.toString();
    }
}
